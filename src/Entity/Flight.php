<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Flight
 *
 * @ORM\Table(name="flights", uniqueConstraints={@ORM\UniqueConstraint(name="flight_id_uindex", columns={"id"}), @ORM\UniqueConstraint(name="flights_number_uindex", columns={"number"})}, indexes={@ORM\Index(name="flights_airports_id_fk_to", columns={"airport_to_id"}), @ORM\Index(name="flights_airports_id_fk_from", columns={"airport_from_id"})})
 * @ORM\Entity(repositoryClass="FlightRepository")
 */
class Flight
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="status", type="boolean", nullable=true, options={"default"="1"})
     */
    private $status = true;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="time", type="time", nullable=true)
     */
    private $time;

    /**
     * @var int|null
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @var \Airports
     *
     * @ORM\ManyToOne(targetEntity="Airport", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="airport_from_id", referencedColumnName="id")
     * })
     */
    private $airportFrom;

    /**
     * @var \Airports
     *
     * @ORM\ManyToOne(targetEntity="Airport", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="airport_to_id", referencedColumnName="id")
     * })
     */
    private $airportTo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(?bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTime(): ?\DateTimeInterface
    {
        return $this->time;
    }

    public function setTime(?\DateTimeInterface $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAirportFrom(): ?Airport
    {
        return $this->airportFrom;
    }

    public function setAirportFrom(?Airport $airportFrom): self
    {
        $this->airportFrom = $airportFrom;

        return $this;
    }

    public function getAirportTo(): ?Airport
    {
        return $this->airportTo;
    }

    public function setAirportTo(?Airport $airportTo): self
    {
        $this->airportTo = $airportTo;

        return $this;
    }


}
