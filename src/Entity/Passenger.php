<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Passenger
 *
 * @ORM\Table(name="passengers", uniqueConstraints={@ORM\UniqueConstraint(name="passengers_id_uindex", columns={"id"}), @ORM\UniqueConstraint(name="passengers_pk", columns={"passport_series", "passport_number"})})
 * @ORM\Entity(repositoryClass="PassengerRepository")
 */
class Passenger
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="passport_series", type="integer", nullable=true)
     */
    private $passportSeries;

    /**
     * @var int|null
     *
     * @ORM\Column(name="passport_number", type="integer", nullable=true)
     */
    private $passportNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="surname", type="string", length=30, nullable=true)
     */
    private $surname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="patronymic", type="string", length=30, nullable=true)
     */
    private $patronymic;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPassportSeries(): ?int
    {
        return $this->passportSeries;
    }

    public function setPassportSeries(?int $passportSeries): self
    {
        $this->passportSeries = $passportSeries;

        return $this;
    }

    public function getPassportNumber(): ?int
    {
        return $this->passportNumber;
    }

    public function setPassportNumber(?int $passportNumber): self
    {
        $this->passportNumber = $passportNumber;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function setPatronymic(?string $patronymic): self
    {
        $this->patronymic = $patronymic;

        return $this;
    }


}
