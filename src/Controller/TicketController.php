<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Repository\FlightRepository;
use App\Repository\PassengerRepository;
use App\Repository\TicketRepository;
use App\Service\MailerService;
use App\Service\ValidationDateService;
use App\Service\PriceCalculatorService;
use App\Service\CreateTicketService;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function count;


class TicketController extends AbstractController
{
    #[Route('/create-ticket', name: 'create_ticket')]
    public function createTicket(
        Request             $request,
        MailerService       $mailerService,
        ValidationDateService $validatorDateService,
        CreateTicketService $createTicketService
    ): Response
    {
//      валидация даты
        if (!$validatorDateService->checkDate($request->request->get('date')))
            return new Response('На выбранную дату рейсов нет', 400);

        $ticket = $createTicketService->createTicket($request);

//      отправка email
        $emailResponse = $mailerService->sendEmail(new Address($ticket->getPassenger()->getSurname() . '@mail.ru'), $ticket);

        return $this->redirectToRoute('ticket', ['id' => $ticket->getId(), 'emailResponse' => $emailResponse->getContent()]);
    }

    #[Route('/ticket/{id}', name: 'ticket')]
    public function showTicket($id, TicketRepository $ticketsRepository, Request $request): Response
    {
        $ticket = $ticketsRepository->find($id);

        return $this->render('ticket/ticket.html.twig', [
            'controller_name' => 'TicketController',
            'ticket' => $ticket,
            'emailResponse' => $request->query->get('emailResponse')
        ]);
    }

    #[Route('/tickets-list', name: 'tickets-list')]
    public function showTicketsList(TicketRepository $ticketsRepository): Response
    {
        $tickets = $ticketsRepository->findAll();

        return $this->render('ticket/list.html.twig', [
            'controller_name' => 'TicketController',
            'tickets' => $tickets
        ]);
    }

    #[Route('/cancel/{id}', name: 'cancel-ticket')]
    public function cancelTicket(ManagerRegistry $doctrine, TicketRepository $ticketsRepository, $id): Response
    {
        $entityManager = $doctrine->getManager();
        $ticket = $ticketsRepository->find($id);
        $ticket->setStatus(0);

        try {
            $entityManager->flush();
        }  catch (Exception $exception) {
            return new Response($exception->getMessage(), 400);
        }

        return $this->redirectToRoute('tickets-list');
    }
}
