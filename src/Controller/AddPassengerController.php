<?php

namespace App\Controller;

use App\Entity\Passenger;
use App\Repository\PassengerRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function count;

class AddPassengerController extends AbstractController
{
    #[Route('/add-passenger', name: 'add_passenger')]
    public function showPassengerForm(): Response
    {
        return $this->render('add_passenger/add_passenger_form.html.twig', [
            'controller_name' => 'AddPassengerController',
        ]);
    }

    #[Route('/create-passenger', name: 'create_passenger')]
    public function createPassenger(ValidatorInterface $validator, Request $request, PassengerRepository $passengersRepository, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $passenger = new Passenger();
        $passenger->setName($request->request->get('name'));
        $passenger->setSurname($request->request->get('surname'));
        $passenger->setPatronymic($request->request->get('patronymic'));
        $passenger->setPassportSeries($request->request->get('passport_series'));
        $passenger->setPassportNumber($request->request->get('passport_number'));

        $errors = $validator->validate($passenger);
        if (count($errors) > 0) {
            return new Response((string) $errors, 400);
        }

        $entityManager->persist($passenger);
        try {
            $entityManager->flush();
        } catch (\Exception $exception) {
            if($exception->getCode() === 1062)
                return new Response('Пассажир с таким паспортом уже существует', 400);
            else
                return new Response($exception->getMessage(), 400);
        }

        return $this->redirectToRoute('reservation');
    }
}
