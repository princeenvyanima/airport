<?php

namespace App\Controller;

use App\Repository\FlightRepository;
use App\Repository\PassengerRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReservationController extends AbstractController
{
    #[Route('/', name: 'reservation')]
    public function showReservationForm(FlightRepository $flightsRepository, PassengerRepository $passengersRepository): Response
    {
        $flights = $flightsRepository
            ->findBy(['status' => 1]);
        $passengers = $passengersRepository
            ->findAll();

        return $this->render('reservation/reservation.html.twig', [
            'controller_name' => 'ReservationController',
            'flights' => $flights,
            'passengers' => $passengers
        ]);
    }

}
