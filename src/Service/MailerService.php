<?php
    namespace App\Service;

    use App\Entity\Ticket;

    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
    use Symfony\Component\Mailer\MailerInterface;
    use Symfony\Component\Mime\Address;
    use Symfony\Component\Mime\Email;


    class MailerService
    {
        private MailerInterface $mailer;

        public function __construct(MailerInterface $mailer)
        {
            $this->mailer = $mailer;
        }

        public function sendEmail(Address $to, Ticket $ticket) :Response
        {
            $email = (new Email())
                ->from('no-replyo@tickets.com')
                ->to($to)
                ->subject('Успешное резервирование билета')
                ->text(
                    'Уважаемый ' . $ticket->getPassenger()->getName()
                    . ' ' . $ticket->getPassenger()->getPatronymic()
                    . ', Ваш рейс №' . $ticket->getFlight()->getNumber() . ' ' . $ticket->getDate()->format('d.m.Y'));

            try {
                $this->mailer->send($email);

            } catch (TransportExceptionInterface $exception) {
                return new Response($exception->getMessage());
            }

            return new Response('Email отправлен');
        }
    }