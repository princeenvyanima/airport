<?php

    namespace App\Service;

    use App\Entity\Ticket;
    use App\Repository\FlightRepository;
    use App\Repository\PassengerRepository;
    use Doctrine\Persistence\ManagerRegistry;
    use Exception;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Validator\Validator\ValidatorInterface;
    use function count;
    use function PHPUnit\Framework\throwException;

    class CreateTicketService
    {
        private FlightRepository $flightsRepository;
        private PassengerRepository $passengersRepository;
        private ManagerRegistry $doctrine;
        private ValidatorInterface $validator;
        private PriceCalculatorService $calculatorService;

        public function __construct(
            FlightRepository    $flightsRepository,
            PassengerRepository $passengersRepository,
            ManagerRegistry     $doctrine,
            ValidatorInterface  $validator,
            PriceCalculatorService $calculatorService,
        )
        {
            $this->flightsRepository = $flightsRepository;
            $this->passengersRepository = $passengersRepository;
            $this->doctrine = $doctrine;
            $this->validator = $validator;
            $this->calculatorService = $calculatorService;
        }

        public function createTicket( $request ): Ticket
        {
            $ticket = new Ticket();
            $ticket->setDate(new \DateTime($request->request->get('date')));
            $ticket->setFlight(
                $this->flightsRepository->find($request->request->get('flight'))
            );
            $ticket->setPassenger(
                $this->passengersRepository->find($request->request->get('passenger'))
            );
            //      установка цены
            $factor = $this->calculatorService->priceCalculator($request->request->get('date'));
            $ticket->setPrice(
                $this->flightsRepository->find($request->request->get('flight'))->getPrice() * $factor
            );
            //      валидация полей
            $errors = $this->validator->validate($ticket);
            if (count($errors) > 0) {
                throw new Exception((string) $errors, 400);
            }
            //      запись в базу
            $entityManager = $this->doctrine->getManager();
            $entityManager->persist($ticket);
            try {
                $entityManager->flush();
            } catch (Exception $exception) {
                throw new Exception($exception->getMessage(), 400);
            }
            return $ticket;
        }
    }