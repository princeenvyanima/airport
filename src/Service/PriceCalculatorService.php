<?php

    namespace App\Service;

    use function date;
    use function strtotime;

    class PriceCalculatorService
    {
        public function priceCalculator($date): int
        {
            $dayofweek = date('w', strtotime($date));

            if( $dayofweek === "6" || $dayofweek === "0" )
                return 2;
            else
                return 1;

        }
    }