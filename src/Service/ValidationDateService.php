<?php

    namespace App\Service;

    use DateTime;
    use phpDocumentor\Reflection\Types\Boolean;

    class ValidationDateService
    {

        public function checkDate($date): bool
        {
            $requestDate = new \DateTime($date);

            if($requestDate < new DateTime())
                return false;
            else
                return true;
        }
    }