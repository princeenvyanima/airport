<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211222151058 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE airports (id INT AUTO_INCREMENT NOT NULL, city_id INT DEFAULT NULL, code VARCHAR(3) DEFAULT NULL, INDEX airports_cities_id_fk (city_id), UNIQUE INDEX airports_id_uindex (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cities (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) DEFAULT NULL, UNIQUE INDEX cities_id_uindex (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE flights (id INT AUTO_INCREMENT NOT NULL, airport_from_id INT DEFAULT NULL, airport_to_id INT DEFAULT NULL, number INT(4) DEFAULT NULL, status TINYINT(1) DEFAULT \'1\', time TIME DEFAULT NULL, price INT DEFAULT NULL, INDEX flights_airports_id_fk_to (airport_to_id), INDEX flights_airports_id_fk_from (airport_from_id), UNIQUE INDEX flight_id_uindex (id), UNIQUE INDEX flights_number_uindex (number), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE passengers (id INT AUTO_INCREMENT NOT NULL, passport_series INT(4) DEFAULT NULL, passport_number INT(6) DEFAULT NULL, name VARCHAR(30) DEFAULT NULL, surname VARCHAR(30) DEFAULT NULL, patronymic VARCHAR(30) DEFAULT NULL, UNIQUE INDEX passengers_id_uindex (id), UNIQUE INDEX passengers_pk (passport_series, passport_number), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tickets (id INT AUTO_INCREMENT NOT NULL, flight_id INT DEFAULT NULL, passenger_id INT DEFAULT NULL, date DATETIME DEFAULT NULL, price INT DEFAULT NULL, status TINYINT(1) DEFAULT \'1\', INDEX tickets_passengers_id_fk (passenger_id), INDEX tickets_flights_id_fk (flight_id), UNIQUE INDEX tickets_id_uindex (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE airports ADD CONSTRAINT FK_6E1AFD608BAC62AF FOREIGN KEY (city_id) REFERENCES cities (id)');
        $this->addSql('ALTER TABLE flights ADD CONSTRAINT FK_FC74B5EA504E723 FOREIGN KEY (airport_from_id) REFERENCES airports (id)');
        $this->addSql('ALTER TABLE flights ADD CONSTRAINT FK_FC74B5EA47AA5690 FOREIGN KEY (airport_to_id) REFERENCES airports (id)');
        $this->addSql('ALTER TABLE tickets ADD CONSTRAINT FK_54469DF491F478C5 FOREIGN KEY (flight_id) REFERENCES flights (id)');
        $this->addSql('ALTER TABLE tickets ADD CONSTRAINT FK_54469DF44502E565 FOREIGN KEY (passenger_id) REFERENCES passengers (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE flights DROP FOREIGN KEY FK_FC74B5EA504E723');
        $this->addSql('ALTER TABLE flights DROP FOREIGN KEY FK_FC74B5EA47AA5690');
        $this->addSql('ALTER TABLE airports DROP FOREIGN KEY FK_6E1AFD608BAC62AF');
        $this->addSql('ALTER TABLE tickets DROP FOREIGN KEY FK_54469DF491F478C5');
        $this->addSql('ALTER TABLE tickets DROP FOREIGN KEY FK_54469DF44502E565');
        $this->addSql('DROP TABLE airports');
        $this->addSql('DROP TABLE cities');
        $this->addSql('DROP TABLE flights');
        $this->addSql('DROP TABLE passengers');
        $this->addSql('DROP TABLE tickets');
    }
}
